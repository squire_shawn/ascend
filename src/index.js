import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
// import registerServiceWorker from './registerServiceWorker';
import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware, createStore } from 'redux';
import reducers from './reducers';
import thunk from 'redux-thunk';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import 'typeface-roboto'



const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));

const getRoutes = () => {
  return (
      <Switch>
        <Route path="/" exact component={App}/>
      </Switch>
  );
};

ReactDOM.render(<Provider store={store}>
      <BrowserRouter>
        {getRoutes(store)}
      </BrowserRouter>
    </Provider>
    , document.getElementById('root'));