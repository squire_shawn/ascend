import { UPLOAD_RECEIVED, OPEN_UPLOAD_MODAL } from '../actions/types';

const initialState = {
  uploadModalState: false
};

export default (state = initialState, action) => {
        console.log("Reducer hit ");
  switch (action.type) {
    case UPLOAD_RECEIVED:
        return { ...state, uploadString: action.value };
    case OPEN_UPLOAD_MODAL:
        return { ...state, uploadModalState: true };
    default:
      return state;
  }
}