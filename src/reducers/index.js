import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import uploadsReducer from './uploadReducer';


const rootReducer = combineReducers({
  uploads: uploadsReducer,
  forms: formReducer
});

const initialState = {
    uploadModalState: false
};

export default rootReducer;