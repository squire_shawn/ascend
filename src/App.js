import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';
import { handleUpload, openUploadModal } from './actions/uploadActions';
import { connect } from 'react-redux';
import ButtonAppBar  from './components/app-bar';
import SimpleModal  from './components/upload-modal';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import theme from './MaterialDashboardTheme/src/assets/css/material-dashboard-react.css';
import { withStyles } from '@material-ui/core/styles';
import GoogleMaps from "./components/full-screen-map";

import Button from '@material-ui/core/Button';


const styles = theme => ({
    uploadModalButton: {
       margin: theme.spacing.unit,
       position: 'absolute',
       top: '8vh',
       left: '4vw',
       zIndex: '12000'
   }
});

class App extends Component {


  render() {
    const{ classes, openUploadModal } = this.props;
    return (
        <div className="App">
        {/*<input type="file" placeholder='file yo' onInput={this.props.getUpload}/>*/}
        {/*<MuiThemeProvider theme={localTheme}>*/}
            <ButtonAppBar/>
            <label htmlFor="raised-button-file">
                    <Button onClick={openUploadModal} color="primary" variant="raised" component="span" className={classes.uploadModalButton}>
                        Upload
                    </Button>
            </label>
            <SimpleModal/>
        <GoogleMaps/>
        </div>
    );
  }
}


App.propTypes = {
    classes: PropTypes.object.isRequired,
};

const AppWrapped = withStyles(styles)(App);

export default connect(null, { handleUpload, openUploadModal })(AppWrapped);

