import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';


const styles = theme => ({
    paper: {
        position: 'absolute',
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[5],
        padding: theme.spacing.unit * 4,
        left: '25vw',
        top: '13vh',
        width: '50vw',
        height: '69vh',
    },
    uploadModalButton: {
        margin: theme.spacing.unit,
        position: 'absolute',
        top: '14em',
        left: '14em',
        zIndex: '12000'
    },
});

class SimpleModal extends Component {

    render() {
        const { classes } = this.props;
        return (<div>
                <Modal
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    open={this.props.open || false}
                    onClose={this.handleClose}
                >
                    <div className={classes.paper}>
                        <Typography variant="title" id="modal-title">
                            Text in a modal
                        </Typography>
                        <Typography variant="subheading" id="simple-modal-description">
                            Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
                        </Typography>
                        <SimpleModalWrapped/>
                    </div>
                </Modal>
                </div>);
    }
}

SimpleModal.propTypes = {
    classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
    console.log("state: ", state);
    return {
      open: state.uploads.uploadModalState
    };
}

// We need an intermediary variable for handling the recursive nesting.
const SimpleModalWrapped = withStyles(styles)(SimpleModal);

export default connect(mapStateToProps, null)(SimpleModalWrapped);
