import { FAILED_UPLOAD_SAVE, SUCCESSFUL_UPLOAD_SAVE, UPLOAD_RECEIVED, OPEN_UPLOAD_MODAL } from './types';
import { getFile } from '../services';
import axios from 'axios';


export const handleUpload = (e) => async (dispatch, getState) => {
  const validImageTypes = [ 'image/gif', 'image/jpeg', 'image/png' ];
  const file = e.target.files[ 0 ];
  if (!validImageTypes.includes(file.type)) {
    return;
  }
  const base64File = await getFile(file)
      .catch(console.error);


  dispatch({ type: UPLOAD_RECEIVED, payload: base64File });
  dispatch(saveFile(base64File));
};


const saveFile = (uploadString) => (dispatch, getState) => {
  const { uploadStatus, error } = axios.post('/api/uploads/new-upload', {
    value: uploadString,
    user: '7e951849-dde6-4f4a-a4ff-c0d6b8a33d24'
  })
      .catch(console.error);
  return uploadStatus ? dispatch({ type: SUCCESSFUL_UPLOAD_SAVE }) :
      dispatch({
        type: FAILED_UPLOAD_SAVE,
        payload: error
      });
};

export const openUploadModal = () => (dispatch, getState) => {
    console.log("action creator hit");
    dispatch({
          type: OPEN_UPLOAD_MODAL
      })
};