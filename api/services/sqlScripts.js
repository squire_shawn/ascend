const { pool } = require('./sqlService');
const _ = require('lodash');
/**
 * @param user GUID
 * @param value {string} A Base64 string consisting of an image as well as associated metadata
 * @param location  {string}
 * @param dateTaken {Date}
 * @param additionalTags {object[]} an array of objects with a `tag` and `value` property
 * */
const insertUpload = async ({ user, value, location = '', dateTaken = new Date(), additionalTags }) => {
  const tagsToPostgresArrays = _.map(additionalTags, a => [ a.tag, a.value ]);
  const statement = `
    INSERT INTO uploads ("user", value, location, date_taken, additional_tags)
    VALUES ($1, $2, $3, $4, $5)
  `;
  
  const values = [ user, Buffer.from(value).toString('base64'), location, dateTaken, tagsToPostgresArrays ];
  const client = await pool.connect();
  return await client.query(statement, values)
      .catch(console.error);
};

module.exports = { insertUpload };