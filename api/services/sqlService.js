const { Pool } = require('pg');
const pool = new Pool({ host: 'database', database: 'ascend', user: 'user', password: 'password' });

pool.on('error', (err, client) => {
  console.error(err, client);
});

testConnection(pool);

module.exports = { pool };

function testConnection(pool) {
  pool.connect()
      .then(client => {
        console.info('Can reach SQL. Ending connection');
        client.release();
      })
      .catch(err => {
        console.error(err);
        console.info('Trying SQL connection again in 5 seconds');
        return setTimeout(() => testConnection(pool), 5000);
      });
}
