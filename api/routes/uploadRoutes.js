const { insertUpload } = require('../services/sqlScripts');
const router = require('express').Router({});


router.post('/new-upload', newUpload);

module.exports = router;

async function newUpload(req, res) {
  const result = await insertUpload(req.body)
      .catch(console.error);
  return res.status(200).json(result);
}