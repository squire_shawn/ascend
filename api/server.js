const express = require('express')
    , app = express()
    , port = 8000
    , UploadRoutes = require('./routes/uploadRoutes');

app.use(express.json({ limit: '200mb' }));

app.use('/api/uploads', UploadRoutes);

app.listen(port, () => console.log(`Server listening on ${port}`));